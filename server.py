from flask import Flask , request, jsonify
import os
import cv2
import sys
import pytesseract
from sklearn.externals import joblib
from skimage.feature import hog
import numpy as np
import re
import requests
from io import BytesIO
from PIL import Image

app = Flask(__name__)


@app.route("/" , methods = ['POST'])
def index():
    response = request.form['link']
    # response = "http://206.189.157.111/api/upload/img/1539503261.jpg"
    response = requests.get(response)

    img = np.asarray(bytearray(response.content), dtype="uint8")
    img = cv2.imdecode(img, cv2.IMREAD_COLOR)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    kernel = np.ones((3,3),np.float32)/9
    gray = cv2.filter2D(gray,-1,kernel)

    edges = cv2.Canny(gray,50,100,apertureSize = 3)

    lines = cv2.HoughLines(edges,1,np.pi/180,200)

    if lines is not None:


        for i in range(len(lines)):

            lines = np.array(lines)

        for i in lines:
            lines = i
            
            x_axis_pos = 0
            y_axis_pos = 0

            for rho, theta in lines:
                a = np.cos(theta)
                b = np.sin(theta)

                x0 = a * rho
                y0 = b * rho

                x1 = int(x0 + 1000 * (-b))
                y1 = int(y0 + 1000 * a)
                x2 = int(x0 - 1000 * (-b))
                y2 = int(y0 - 1000 * a)

                if b > 0.5:
                    # Check the psoition of the line
                    if rho - x_axis_pos > 10:
                        x_axis_pos = rho
                        # Draw a line along the edge
                        cv2.line(gray, (x1, y1), (x2, y2), (255, 255, 255), 2)
                else:
                    if rho - y_axis_pos > 10:
                        y_axis_pos = rho
                        cv2.line(gray, (x1, y1), (x2, y2), (255, 255, 255), 2)
    

 
    clf = joblib.load("digits_cls.pkl")


    im_gray = cv2.bilateralFilter(gray, 9 , 75 , 75);

    
    ret, im_th = cv2.threshold(im_gray, 90, 255, cv2.THRESH_BINARY_INV)

    im_th = cv2.dilate(im_th,kernel,iterations = 1)

    
    _,ctrs, hier = cv2.findContours(im_th.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Get rectangles contains each contour
    rects = [cv2.boundingRect(ctr) for ctr in ctrs]

    final = [];
    sorted_by_second = sorted(rects, key=lambda tup: (tup[1] , tup[0]))


    final.extend(sorted(sorted_by_second[0 : 3], key=lambda tup: (tup[0])))
    final.extend(sorted(sorted_by_second[3 : 6], key=lambda tup: (tup[0])))
    final.extend(sorted(sorted_by_second[6 : 9], key=lambda tup: (tup[0])))

    data = [];

    for rect in final:
        # Draw the rectangles
        # cv2.rectangle(im, (rect[0], rect[1]), (rect[0] + rect[2], rect[1] + rect[3]), (0, 255, 0), 3) 
        # Make the rectangular region around the digit
        leng = int(rect[3] * 1.6)
        pt1 = int(rect[1] + rect[3] // 2 - leng // 2)
        pt2 = int(rect[0] + rect[2] // 2 - leng // 2)
        roi = im_th[pt1:pt1+leng, pt2:pt2+leng]
        # Resize the image
        roi = cv2.resize(roi, (28, 28), interpolation=cv2.INTER_AREA)
        roi = cv2.dilate(roi, (3, 3))
        # Calculate the HOG features
        roi_hog_fd = hog(roi, orientations=9, pixels_per_cell=(14, 14), cells_per_block=(1, 1), visualise=False)
        nbr = clf.predict(np.array([roi_hog_fd], 'float64'))

        data.extend(str(int(nbr[0])));
        # cv2.putText(im, str(int(nbr[0])), (rect[0], rect[1]),cv2.FONT_HERSHEY_DUPLEX, 2, (0, 0, 255), 3)
    


    dataSeg = (chunkIt(data, 3))
    print(dataSeg)
    dataSeg[0] = map(int, dataSeg[0])
    dataSeg[1] = map(int, dataSeg[1])
    dataSeg[2] = map(int, dataSeg[2])


    listDataFinal = [];
    listDataFinal.extend(dataSeg)

    dataFinal = {}
    dataFinal['number'] = listDataFinal;
    dataFinal['answer'] = is_magic(listDataFinal)

    
    return jsonify({"res" : dataFinal})


def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out

def is_magic(square):
    magic_sum = 0
    check_sum = 0
    for x in range(len(square)):
        check_sum += square[0][x]
    # check for the sum of the first row as a control sum
    
    for y in range(len(square)):
        for x in range(len(square)):
            magic_sum += square[x][y]
        if magic_sum != check_sum:
            return False
        else:
            magic_sum = 0
    #rows checked
    
    for y in range(len(square)):
        for x in range(len(square)):
            magic_sum += square[y][x]
        if magic_sum != check_sum:
            return False
        else:
            magic_sum = 0
    # columns checked
    
    for x in range(len(square)):
        magic_sum += square[x][x]
    if magic_sum != check_sum:
        return False
    else:
        magic_sum = 0
    # diagonal checked

    for x in range(len(square)):
        magic_sum += square[len(square)-x-1][x]
    if magic_sum != check_sum:
        return False
    else:
        magic_sum = 0
    # contrdiagonal checked
    
    return True

if __name__ == '__main__':
    app.run(host='0.0.0.0' , port=8181)
